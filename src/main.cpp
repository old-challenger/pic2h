// ---------------------------------------------------------------------------------------------------------
#include "filesParser.h"
// ---------------------------------------------------------------------------------------------------------
int main ( int argsSize_p, char ** args_p ) {
	if ( argsSize_p < 2 ) {	
      std::cout << "\n[ERROR] Please specify minimum one parameter.\n"; return 1; 
   }
	parser parser_l;
	if ( !parser_l.parseArgs( argsSize_p, args_p ) ) {
		for ( std::size_t i = parser_l.errors().size(); i != 0 ; )
			std::cout << "\n[ERROR] " << parser::errorString( parser_l.errors().at( --i ) );
		std::cout << "\n";
		return 2;
	}
	parser_l.go();
	std::cout << "\n";
	return 0;
}
// ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------