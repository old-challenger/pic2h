// ---------------------------------------------------------------------------------------------------------
#ifndef FILE_PARSER_H_MRV
#define FILE_PARSER_H_MRV
// ---------------------------------------------------------------------------------------------------------
#include <time.h>
// ---------------------------------------------------------------------------------------------------------
#include <list>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <cassert>

class parser {
public:
	enum eErros {
		NO_ERRORS               = 0,
		ERR_INVALID_KEY_USING   = 1,
		ERR_NO_IN_FILES         = 2,
		ERR_CAN_NOT_OPEN_IN     = 3,
		ERR_CAN_NOT_OPEN_OUT    = 4,
		ERR_CAN_NOT_OPEN_LIST   = 5
	};
	// ------------------------------------------------------------------------------------------------------
	static const std::size_t errorsSize_sc = 6;
	// ------------------------------------------------------------------------------------------------------
private:

	enum eLineType {
	   LINE_BIG	   = 0,
		LINE_MIDLE	= 1,
		LINE_SMALL	= 2
	};
   // ------------------------------------------------------------------------------------------------------

   enum parametersTypes_enum {
      PTYPE_PICTURE_FILE   = 0,
      PTYPE_LIST_FILE      = 1,
      PTYPE_OUT_FILE       = 2
   };
   // ------------------------------------------------------------------------------------------------------

	std::string outFileName;
   std::vector< eErros > errorArr;
	std::list< std::string > inFilesList;
	// ------------------------------------------------------------------------------------------------------

	static const std::string & lineTxt ( eLineType type_p ) {
		switch ( type_p ) {
			case LINE_BIG: {
				static const std::string line_l = 
               "// ---------------------------------------------------"
               "------------------------------------------------------";
				return line_l;
			}
			case LINE_MIDLE: {
				static const std::string line_l = 
               "// -------------------------------------------------"
               "-----------------------------------------------------";
				return line_l;
			}
			case LINE_SMALL: {
				static const std::string line_l = "";
				return line_l;
			}
			default: {
				static const std::string line_l = "";
				return line_l;
			}
		}	// switch ( type_p ) ..
	}
	// ------------------------------------------------------------------------------------------------------

	template < typename numeric_t >
	static std::string makeHexStr( numeric_t number_p ) {
		std::stringstream strStream_l;
		strStream_l << std::hex << number_p;
		const std::string result_l = result_l.length() % 2 
			? std::string( "0" ) + strStream_l.str() : strStream_l.str();
		return std::move( result_l );
	}
	// ------------------------------------------------------------------------------------------------------
	
	static std::string makeLexemeByFileName ( const std::string & fileName_p, 
                                                   std::string * extension_p = NULL ) {
		std::string result_l( fileName_p ); 
		const std::size_t lastSymbol_l = result_l.length() - 1;
		for ( std::size_t i = lastSymbol_l; i >= 0; --i )
			if ( '.' == result_l[ i ] ) { 
            if ( NULL != extension_p ) 
               *extension_p = i < lastSymbol_l ? result_l.substr( i + 1, lastSymbol_l - i ) : "";
            result_l.erase( i ); 
            break; 
         }
		return result_l;
	}
	// ------------------------------------------------------------------------------------------------------

   void readListFile ( const std::string & fileName_p ) {
      std::ifstream inFile_l;
      inFile_l.open( fileName_p.c_str(), std::ios::in );
      if ( !inFile_l.is_open() ) {
         errorArr.push_back( ERR_CAN_NOT_OPEN_LIST );
         return ;
      }
      static const std::streamsize buffSize_l = 1024;
      char buffer_l[ buffSize_l ];
      while ( !inFile_l.eof() ) {
         inFile_l.getline( buffer_l, buffSize_l );
         inFilesList.push_back( buffer_l );
      }
      inFile_l.close();
   }
   // ------------------------------------------------------------------------------------------------------

	bool writeFileHeader () {
		// �������� ����
		std::ofstream outFile_l;
		outFile_l.open( outFileName.c_str(), std::ios::out );
		if ( !outFile_l.is_open() ) return false;
      std::string className_l = makeLexemeByFileName( outFileName );
      outFile_l	<<	lineTxt( LINE_BIG ).c_str()
                  <<	"\n#ifndef ICONS_H_MRV"
                     "\n#define ICONS_H_MRV\n"
                  <<	lineTxt( LINE_BIG ).c_str()
                  <<	"\n#include <uIcons.h>\n"
                  << lineTxt( LINE_BIG ).c_str()
                  <<	"\nclass " << className_l
                  << "\n{\nprivate:\n\t"
                  << className_l << " () {}\n\t"
                  <<	lineTxt( LINE_MIDLE ).c_str()
                  <<	"\npublic:";
		outFile_l.close();
		return true;
	}
	// ------------------------------------------------------------------------------------------------------

	bool writeFileEnd () {
		// �������� ����
		std::ofstream outFile_l;
		outFile_l.open( outFileName.c_str(), std::ios::out | std::ios::app );
		if ( !outFile_l.is_open() ) return false;
		outFile_l	<< "\n};\t// class icons\n"
					   << lineTxt( LINE_BIG ).c_str()
					   << "\n#endif // ICONS_H_MRV\n"
					   << lineTxt( LINE_BIG ).c_str();
		outFile_l.close();
		return true;
	}
	// ------------------------------------------------------------------------------------------------------

	eErros readNextFile ( const std::string & fileName_p ) {
		// �������� ����
		std::ofstream outFile_l;
		outFile_l.open( outFileName.c_str(), std::ios::out | std::ios::app );
		if ( !outFile_l.is_open() ) {	return ERR_CAN_NOT_OPEN_OUT; }
		// �������� ����
		std::ifstream inFile_l;
		inFile_l.open( fileName_p.c_str(), std::ios::in | std::ios::binary );
      if ( !inFile_l.is_open() ) { outFile_l.close(); return ERR_CAN_NOT_OPEN_IN; }
		// ������ ��������� �������
      std::string extension_l;
      const std::string functionName_l = static_cast< std::string >( "static const utl::icon & ")
                                          + makeLexemeByFileName( fileName_p, &extension_l ) + " ()";
      outFile_l   << "\n\n\t" << functionName_l.c_str()
		            << "\n\t{\n\t\tstatic const std::size_t iconSize_slc = ";
		// ����������� ������� �����
		inFile_l.seekg ( 0, std::ios_base::end );
		std::streamsize fileSize_l = inFile_l.tellg();
		inFile_l.seekg( 0, std::ios_base::beg );
		// ������ ������� � ����
		outFile_l << fileSize_l << ";\n\t\t";
		// ������ �����
		const std::streamsize buffSize_l( 1024 );
		unsigned char inBuffer_l[ buffSize_l ];
		std::string outString_l = "static const unsigned char iconArray_slc [ iconSize_slc ] = {";
		std::streamsize   nextReadSize_l = -1,
                        bytesWrited_l = 0;
		while ( nextReadSize_l ) {
			nextReadSize_l = std::min< std::streamsize >( fileSize_l - inFile_l.tellg(), buffSize_l );
			inFile_l.read( ( char * ) inBuffer_l, nextReadSize_l );
			for ( std::streamsize i = 0; i < nextReadSize_l; ) {
            if ( !( bytesWrited_l++ % 0x0f ) ) outString_l += "\n\t\t\t";
				outString_l += "0x";
				outString_l += makeHexStr( static_cast< unsigned short int >( inBuffer_l[ i++ ] ) ) + ", ";
			}
		}
		inFile_l.close();
		outString_l.erase( outString_l.length() - 2, 2 );
      outString_l +=	static_cast< std::string >( 
                        " };\n\t\tstatic const utl::icon icon_slc ( iconArray_slc, iconSize_slc, \"" ) 
                     + extension_l + "\" );\n\t\treturn icon_slc;\n\t}\t// " + functionName_l + "\n\t";
		outFile_l << outString_l;
		outFile_l << lineTxt( LINE_MIDLE ).c_str();
		outFile_l.close();
		return NO_ERRORS;
	}
	// ------------------------------------------------------------------------------------------------------
public:

	static const std::string & errorString ( std::size_t error_p ) {
		static const std::string emtyStr_l = "";
		static const std::string errorString_scl[ errorsSize_sc ] = 
			{	"No Errors", "Invalid key using", "In file name is't specified", 
            "Can't open one of in files ", "Can't open out file", "Can't open file with list of pics" };
		return error_p < errorsSize_sc ? errorString_scl[ error_p ] : emtyStr_l;
	}
	// ------------------------------------------------------------------------------------------------------

   parser () : errorArr(), outFileName(), inFilesList() { ; }
	// ------------------------------------------------------------------------------------------------------

	const std::vector< eErros > & errors () const {	return errorArr; }
	// ------------------------------------------------------------------------------------------------------

	bool parseArgs ( int argsSize_p, char ** args_p ) {
		errorArr.clear();
		inFilesList.clear();
		outFileName.clear();
		
		parametersTypes_enum nextParameter_l = PTYPE_PICTURE_FILE;
		for ( int i = 1; i < argsSize_p; ++i ) {
			std::string	currName_l = args_p[ i ];
			if ( "-o" == currName_l ) {
				outFileName.clear();
				nextParameter_l = PTYPE_OUT_FILE;
			}
         else if ( "-f" == currName_l ) nextParameter_l = PTYPE_LIST_FILE;
         else {
            if ( PTYPE_OUT_FILE == nextParameter_l ) 
               outFileName = currName_l;
            else if ( PTYPE_LIST_FILE == nextParameter_l ) 
               readListFile( currName_l );
			   else 
               inFilesList.push_back( currName_l );
            nextParameter_l = PTYPE_PICTURE_FILE;
         }
		}
   	if ( PTYPE_PICTURE_FILE != nextParameter_l ) 
         errorArr.push_back( ERR_INVALID_KEY_USING );
      if ( outFileName.empty() ) {
			outFileName = "pic_";
			outFileName += makeHexStr( static_cast< int >( time( NULL ) ) );
			outFileName += ".h";
		}
		if ( inFilesList.empty() ) 
         errorArr.push_back( ERR_NO_IN_FILES );
		return errorArr.empty();
	}
	// ------------------------------------------------------------------------------------------------------

	bool go () {
		if ( writeFileHeader() ) 
			for (	std::list< std::string >::iterator it = inFilesList.begin();
					inFilesList.end() != it; ++it ) {
				eErros nextResult_l = readNextFile( *it );
				if ( NO_ERRORS != nextResult_l ) 
               errorArr.push_back( nextResult_l );
			}
		else errorArr.push_back( ERR_CAN_NOT_OPEN_OUT );
		if ( !writeFileEnd () ) 
         errorArr.push_back( ERR_CAN_NOT_OPEN_OUT );
		return errorArr.empty();
	}
	// ------------------------------------------------------------------------------------------------------
}; // class parser
// ---------------------------------------------------------------------------------------------------------
#endif // FILE_PARSER_H_MRV
// ---------------------------------------------------------------------------------------------------------